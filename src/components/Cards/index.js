import React from 'react';
import Card from '../Card';
import './cards.css';
import PropTypes from 'prop-types';

class Cards extends React.Component {
    
    constructor(props) {
        super(props);
    }

    render(){

        return(
            <>
                <div className='cards_wrapper'>{this.props.cards.map((elem) => 
                    <Card 
                    card={elem} 
                    key={elem.article}
                    favoriteCards = {this.props.favoriteCards.includes(elem)}
                    favorite = {this.props.favorite}
                    clickOpenModal = {this.props.clickOpenModal}
                    addToFavorites = {this.props.addToFavorites}
                    />)} 
                </div>
            </>   
        )
    }
}

Cards.propTypes = {
    favoriteCards: PropTypes.array,
    clickOpenModal: PropTypes.func,
    addToFavorites: PropTypes.func,
}

export default Cards;