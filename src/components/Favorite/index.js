import React from 'react';
import starunfilled from '../../image/starunfilled.png';
import './favorite.css';
import PropTypes from 'prop-types';

class Favorite extends React.Component {

    constructor(props){
        super(props);   
    }

    render() {

        return(
            <div className='favorite_wrapper'>
                <a>
                    <img src={starunfilled}></img>
                </a>
                <div className='favorite-count'>
                {this.props.favoriteCards.length}
                </div>
            </div>
        )
    }
}

Favorite.propTypes = {
    name: PropTypes.string,
    article: PropTypes.number,
    price: PropTypes.number,
    url: PropTypes.string,
    color: PropTypes.string
  };

export default Favorite;