import React from "react";
// import Button from "../Button";
import Modal from '../Modal';
import Cards from '../Cards';
import Basket from '../Basket';
import iconstore from '../../image/iconstore80.png';
import Favorite from '../Favorite';
import './app.css';

class App extends React.Component {
    
    constructor() {
        super();

        this.state = {
            openModal: false,
            modal: {},
            cards: [],
            basket: [],
            favoriteCards: [],
            favorite: false
        }

        this.clickOpenModal = this.clickOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.addToFavorites = this.addToFavorites.bind(this);

        this.modalsData = {
            red: {
                header: "Do you want to delete this file?",
                text: "Once you delete this file, it won't be possible to undo this action. Are you sure you want delete it?",
                actions1: "OK",
                actions2: "Cancel"
            }, 

            green: {
                header: "Do you want to save it?",
                text: "If not, it will be delete, choose an option:",
                actions1: "Save",
                actions2: "Delete"
            },

            addCard: {
                text: "Are you sure you want add it?",
                actions1: "Confirm",
                actions2: "Cancel",
                // onAction1: this.addToBasket,
                onAction2: this.closeModal,
                // onAction3: this.addToFavorites,
            },

            editBasket: {
                onAction1: this.editBasket,
                onAction2: this.closeModal,
            }
        }
    }

    addToFavorites(card) {
        let favoritesArray = this.state.favoriteCards;
        favoritesArray.push(card);

        this.setState({
            favoriteCards: favoritesArray,          
        })
        favoritesArray.includes(card) ? this.state.favorite = true : this.state.favorite=false

        localStorage.setItem('Favorite Toys', JSON.stringify(favoritesArray));
    }

    addToBasket(card) {
        let basketArray = this.state.basket;
        basketArray.push(card);
        this.setState({
             basket: basketArray,
        })
       
        localStorage.setItem('Toys basket', JSON.stringify(basketArray));
     
    }    

    componentDidMount(card) {
        fetch('card.json')
            .then((res) => res.json())
            .then((data) =>  this.setState({cards:data}))  
            
        let basketArrayRaw = localStorage.getItem (['Toys basket']) ;
        let basketArray  = basketArrayRaw ? JSON.parse(basketArrayRaw) : [];
        this.setState( {basket: basketArray });

        let favoriteArrayRaw = localStorage.getItem (['Favorite Toys']) ;
        let favoriteCards  = favoriteArrayRaw ? JSON.parse(favoriteArrayRaw) : [];
        this.setState( {favoriteCards: favoriteCards });
    }

    clickOpenModal(modalId, card) {
        let modal = this.modalsData[modalId];
        modal.header = "In the basket: " + card.name;
        modal.onAction1 = () => { 
            this.closeModal();
            this.addToBasket(card);
        };

        this.setState({
            modal: modal,
            openModal: true
        })
    }

    closeModal() {
        this.setState({
            openModal: false,
        })
    }

    render() {
       
        return (
            <>  
                <header>
                    <img src={iconstore}></img>
                    <div className='counts_wrapper'>
                        <Favorite favoriteCards = {this.state.favoriteCards}/>
                        <Basket basket = {this.state.basket}/>
                    </div>    
                </header>
                <div>  
                    <Cards cards = {this.state.cards} 
                    favoriteCards = {this.state.favoriteCards}
                    favorite = {this.state.favorite}
                    clickOpenModal = {this.clickOpenModal}
                    addToFavorites = {this.addToFavorites}
                     /> 
                    <Modal 
                    modalsData = {this.state.modal}
                    header = {this.state.modal.header}
                    text = {this.state.modal.text}
                    open = {this.state.openModal} 
                    onClose = {this.closeModal}
                    onAction1 = {this.state.modal.onAction1}
                    onAction2 = {this.state.modal.onAction2}
                    actions1 = {this.state.modal.actions1} 
                    actions2 = {this.state.modal.actions2}
                    />
                </div>
            </>
        )
    }
}

export default App;