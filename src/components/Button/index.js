//import { render } from "@testing-library/react";
import React from 'react';
import './button.css';
import PropTypes from 'prop-types';


class Button extends React.Component {

    constructor(props) {
        super(props);
        
        this.openModal = this.openModal.bind(this);
    }

    openModal() {
        this.props.onClick(this.props.modalId, this.props.card);
    }

    render(){

        return(
            <button onClick={this.openModal} 
            style={{backgroundColor: this.props.backgroundColor, 
                    padding: '10px 15px'}} >
                {this.props.text} 
            </button>
        )
    }
}

Button.propTypes = {
    text: PropTypes.string,
    card: PropTypes.object,
  };

Button.defaultProps = {
    backgroundColor: 'rgb(238, 238, 238)',
};

export default Button;