import React from 'react';
import basket from '../../image/basket.png';
import './basket.css';
import PropTypes from 'prop-types';

class Basket extends React.Component {

    constructor(props){
        super(props);   
    }

    render() {

        return(
            <div className='basket_wrapper'>
                <a className='basket-icon'>
                    <img src={basket}></img>
                </a>
                <div className='basket-count'>
                {this.props.basket.length}
                </div>
            </div>
        )
    }
}

Basket.propTypes = {
    basket: PropTypes.array,
  };

export default Basket;