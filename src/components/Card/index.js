import React from 'react';
import './card.css';
import Button from '../Button';
import starunfilled from '../../image/starunfilled.png';
import starfilled from '../../image/starfilled.png';
import PropTypes from 'prop-types';


class Card extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 
            favorite: false
        };

        this.addToFavorites = this.addToFavorites.bind(this);
    };

    addToFavorites(){
        this.props.addToFavorites(this.props.card);
    }

    isFavorite (card) {

    }

    render (){
        
        return(
            <div className='card'>
                <div style={{textAlign: 'center', marginTop: '15px'}}>
                    <img src={this.props.card.url} className='imgCard'></img>
                </div>
                <h1>{this.props.card.name}</h1>
                <h3>Price: {this.props.card.price}.00 euro</h3>
                <hr/>
                <p>Article: {this.props.card.article}</p>
                <p>Color: {this.props.card.color}</p>
                <div className='add_wrapper'>
                    <div className='button_wrapper'>
                        <Button text='Add to cart' onClick={this.props.clickOpenModal} modalId='addCard' card={this.props.card} backgroundColor='#bdbdbd'/>
                    </div>
                    <div>
                        <a onClick={this.addToFavorites} >
                            <img src=  {!this.props.favoriteCards ? starunfilled : starfilled } > 
                            </img> 
                        </a>
                    </div>
                </div>

            </div>
        )
    }
}

Card.propTypes = {
    name: PropTypes.string,
    article: PropTypes.number,
    price: PropTypes.number,
    url: PropTypes.string,
    color: PropTypes.string
};

Card.defaultProps = {
    article: 0,
};

export default Card;


