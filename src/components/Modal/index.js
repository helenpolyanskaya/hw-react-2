import React from 'react';
import './modal.css';
import PropTypes from 'prop-types';

class Modal extends React.Component {

    constructor(props) {
        super(props);

        this.onClose = this.onClose.bind(this);
    }
    
    onClose() {
        this.props.onClose()
    }

    showClass() {
        if(this.props.open === false){
            return 'off';
        }   
        return "";   
    }

    render() {

        if(this.props.open === false){
            return null;
        }

        return (
         <div  id='modal_wrapper' onClick={(e) => e.target === e.currentTarget && this.onClose()}>
            <div className={`modal ${this.showClass()}`}>
                <div className='header_wrapper'>
                    <h2>{this.props.header}</h2>
                    <button className='toggle-button' onClick={this.onClose}>
                        X
                    </button>
                </div>
                <div className='content'>
                    <h3>{this.props.text}</h3>
                    <button onClick = {this.props.onAction1} style={{border: '1px solid'}}>
                        {this.props.actions1}
                    </button>
                    <button onClick = {this.props.onAction2} style={{border: '1px solid'}}>
                        {this.props.actions2}
                    </button>
                </div>
            </div>   
         </div>
            
        )
    }
}

let buttons = document.querySelectorAll('.toggle-button');
let modal = document.querySelector('#modal');
[].forEach.call(buttons, function (button) {
    button.addEventListener('click', function () {
        modal.classList.toggle('off');
    });
});

Modal.propTypes = {
    actions1: PropTypes.string,
    actions2: PropTypes.string,
  };

export default Modal;

 
